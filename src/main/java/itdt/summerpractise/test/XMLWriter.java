package itdt.summerpractise.test;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;


import java.io.File;
import java.util.*;

class XMLWriter {

    private Document makeDoc(Map<String, String> fields, ArrayList<Map<String, String>> rowDataList ) throws ParserConfigurationException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        Document doc = null;

            doc = factory.newDocumentBuilder().newDocument();
            Element datapacket = doc.createElement("DATAPACKET");
            Element metadata = doc.createElement("METADATA");
            datapacket.appendChild(metadata);
            doc.appendChild(metadata);
            Element fields1 = doc.createElement("FIELDS");
            metadata.appendChild(fields1);

            for (String key : fields.keySet())
            {
                Element field = doc.createElement("FIELD");
                fields1.appendChild(field);
                field.setAttribute("DisplayLabel", key);
                field.setAttribute("FieldClass", "TField");
                field.setAttribute("FieldName", key);
                field.setAttribute("FielType", fields.get(key));
            }
            Element rowdata = doc.createElement("ROWDATA");
            metadata.appendChild(rowdata);

            for (Map<String, String> rowData:rowDataList)
            {   Element row = doc.createElement("Row");
                rowdata.appendChild(row);
                for (String keyRowData:rowData.keySet()) {
                    row.setAttribute(keyRowData, String.valueOf(rowData.get(keyRowData)));
                }
            }

        return doc;
    }

    void writeDoc(String pathdoc, Map<String, String> fields, ArrayList<Map<String, String>> rowDataList) throws TransformerException, ParserConfigurationException {
        Document doc = this.makeDoc( fields, rowDataList);
        File file = new File(pathdoc);
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(new DOMSource(doc), new StreamResult(file));

    }


}
