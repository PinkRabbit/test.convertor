package itdt.summerpractise.test;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.*;

class Hendler {


    private MyDbfReader createRecordsFromDbf() throws IOException {
        System.out.println("Введите полный путь к файлу, вместе с названием файла");
        try( InputStreamReader input = new InputStreamReader(System.in);
                BufferedReader buff = new BufferedReader(input);
                FileInputStream file =  new FileInputStream(buff.readLine())){
            MyDbfReader reader = new MyDbfReader(file ,Charset.forName("Cp866"));
            System.out.println("Колонки в документе " + reader.getFields().keySet());
        return reader;}
    }

    private class DelFields {
        ArrayList<Map<String, String>> rowDataList = new ArrayList<>();
        Map<String, String> newFields = new HashMap<>();

        ArrayList<Map<String, String>> getRowList(String[] removeList, ArrayList<Map<String, String>> rowList, Map<String, String> fields){
            for(String removeListItem : removeList){
                for (Map<String, String> rowData:rowList){
                    Map<String, String> newRow = new HashMap<>();
                    for(String keyRecord:rowData.keySet()){
                        if(!removeListItem.equals(keyRecord)){newRow.put(keyRecord, fields.get(keyRecord));}
                    }
                    rowDataList.add(newRow);
                }
            }

            return rowDataList;
        }
        Map <String, String> getFields(String[] removeList, Map<String, String> fields){

            for(String removeListItem : removeList){
                for (String key: fields.keySet())
                    if (!removeListItem.equals(key)) {
                        newFields.put(key, fields.get(key));
                    }
            }
            return newFields;
        }

    }


    private String[] createRemoveList() throws IOException {
        System.out.println("Введите названия колонок которые хотите удалить с учетом регистра, разделитель - пробел");
        BufferedReader removeStream = new BufferedReader(new InputStreamReader(System.in));
        String remove = removeStream.readLine();
        String delimiter = " ";
        return remove.split(delimiter);
    }

    private void buildDoc(Map<String, String> fields, ArrayList<Map<String, String>> rowDataList) throws IOException, TransformerException, ParserConfigurationException {
        System.out.println("Введите название сохраняемого файла с расширением .xml");
        BufferedReader xmlInput = new BufferedReader(new InputStreamReader(System.in));
        String pathnameXML = xmlInput.readLine();
        XMLWriter writer = new XMLWriter();
        writer.writeDoc(pathnameXML, fields, rowDataList);
    }

    void process() throws ParserConfigurationException, TransformerException, IOException {
        try(MyDbfReader reader = this.createRecordsFromDbf()) {
            String[] removeList = this.createRemoveList();
            DelFields delFields= new DelFields();
            ArrayList<Map<String, String>> rowDataList = delFields.getRowList(removeList, reader.getRowData(), reader.getFields());
            Map<String, String> fields = delFields.getFields(removeList, reader.getFields());
            this.buildDoc(fields, rowDataList);
        }
    }

}
