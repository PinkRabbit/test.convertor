package itdt.summerpractise.test;

import com.linuxense.javadbf.*;
import java.io.*;
import java.nio.charset.Charset;
import java.util.*;
import java.util.stream.IntStream;


class MyDbfReader extends DBFReader {

    MyDbfReader(FileInputStream fileInputStream, Charset charset) {
        super(fileInputStream, charset);
    }
    Map<String, String> getFields(){
        Map <String, String> fields = new HashMap<>();
        IntStream.range(0,getFieldCount())
                    .forEach(i->fields.put(getField(i).getName(),getField(i).getType().toString()));
        return fields;
    }
    ArrayList<Map<String, String>> getRowData()  {
        ArrayList<Map<String, String>> rowData = new ArrayList<>();
        Object[] rowObjects;//без Object[] не обойтись,
                           //т.к. DBFReader.nextRecord()предсталяет записть как массив обектов разных типов

        while ((rowObjects = nextRecord()) != null)
        {
            Map<String,String> rowDict = new HashMap<>();
            Object[] finalRowObjects = rowObjects;//здесть нужна локальная копия для использования ее в лямба-методе
            IntStream.range(0,getFieldCount())
                    .forEach(i-> rowDict.put(getField(i).getName(), finalRowObjects[i].toString()));//записть здесть в Map и потом в xml происходит по элеметно
            rowData.add(rowDict);                                                                  //и для записы нужна именно страка, поэтому привожу к строке
        }
        return rowData;
    }
}